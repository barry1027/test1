*** Settings ***
Library  Selenium2Library
Library  SSHLibrary
Library  RequestsLibrary
Library  String

***Variables***
${account}  d90218
${password}  Barry1027
${sleep_time}  8s
${to_con}  d90218@gamil.com
${main_con}  test
${con}  123456789987654321

*** Keywords ***
Get Xpath
    ${write}=  Get Source
    ${result}=  Should Match Regexp  ${write}  div id=":([\\w]+)" class="Am Al editable LW-avf"
    Set Suite Variable  ${con_id}  ${result[1]}

Get Send Xpath
    ${send_m}=  Get Source
    ${fe}=  Should Match Regexp  ${send_m}  div id=":([\\w]+)" class="T-I J-J5-Ji aoO T-I-atl L3"
    Set Suite Variable  ${send_id}  ${fe[1]}



*** Test Cases ***
Open browser and run
  Open Browser  https://mail.google.com/mail/u/0/#inbox  chrome
  Import Variables  D:\\Robot1\\var3.py
  Input text  id=Email  ${account}
  Click element  id=next
  Wait Until Element Is Visible  xpath=${ENTER_PASSWORD_XPATH}
  Input text  ${ENTER_PASSWORD_XPATH}  ${password}
  Wait Until Element Is Visible  xpath=${Sign_In_Xpath}
  Click element  xpath=${Sign_In_Xpath}
  Wait Until Page Contains  ${mail_URL}  ${sleep_time}
  Wait Until Element Is Visible  xpath=${Write_Button}
  Click element  xpath=${Write_Button}
  Wait Until Element Is Visible  name=to
  Input text  name=to  ${to_con}
  wait until Element Is Visible  name=subjectbox
  Input text  name=subjectbox  ${main_con}
  Get Xpath
  Input text  id=:${con_id}  ${con}
  Get Send Xpath
  Wait Until Element Is Visible  id=:${send_id}
  Click Element  id=:${send_id}